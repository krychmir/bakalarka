#ifndef _EPD_H_
#define _EPD_H_

#include "DEV_Config.h"
#include "GUI_Paint.h"
#include "GUI_BMPfile.h"
#include "ImageData.h"
#include "Debug.h"
#include <stdlib.h>

typedef struct Menu{
  
  void * parent;
  void ** children;
  int nChildren;
  char * name;

} Menu; 

int getSelected();

int EPD_2in7_init(int * menuSize, char ** menuName);
int EPD_2in7_draw_menu(Menu * menu, int state);
int EPD_2in7_draw_currMenu();
char * EPD_2IN7_curentMenuName();
int EPD_2IN7_input(int input);
int EPD_2in7_free();

void EPD_2in7_test(void);

Menu * menu_init(Menu * parent, int * menuSize, char ** menuName, int inIndex);
Menu * get_subMenu(int * menuNum, Menu * menu);
void menu_free(Menu * menu);

#endif
