#include "EPD.h"
#include "EPD_2in7.h"
#include <time.h>
#include <wiringPi.h>

//DISP_RES 264x176 {1...264}x{1...176}

UBYTE *BlackImage;

Menu * mainMenu;
Menu * curentMenu;

int selected = 0;

int getSelected(){
  return selected;
}

int EPD_2in7_init(int * menuSize, char ** menuName)
{
  printf("DEV_Module_Init\n");
  if(DEV_Module_Init() != 0){
    return -1;
  }

  printf("e-Paper Init and Clear...\n");
  EPD_2IN7_Init();
	
  
  UWORD Imagesize = ((EPD_2IN7_WIDTH % 8 == 0)? (EPD_2IN7_WIDTH / 8 ): (EPD_2IN7_WIDTH / 8 + 1)) * EPD_2IN7_HEIGHT;
  if((BlackImage = (UBYTE *)malloc(Imagesize)) == NULL) {
    printf("Failed to apply for black memory...\n");
    return -2;
  }

  printf("Menu init\n");
  mainMenu = menu_init(NULL, menuSize, menuName, 1);
  curentMenu = mainMenu;

  return 0;
}

void EPD_2in7_test(){

  char buff[50];
  time_t t = time(NULL);
  struct tm tm = *localtime(&t);
  sprintf(buff, "%d-%02d-%02d %02d:%02d:%02d", tm.tm_year + 1900, tm.tm_mon + 1, tm.tm_mday, tm.tm_hour, tm.tm_min, tm.tm_sec);

  printf("Paint_NewImage\r\n");
  Paint_NewImage(BlackImage, EPD_2IN7_WIDTH, EPD_2IN7_HEIGHT, 270, WHITE);
  Paint_SelectImage(BlackImage);
  Paint_Clear(WHITE);
  
  Paint_DrawLine(1, 30, 264, 30, BLACK, DOT_PIXEL_1X1, LINE_STYLE_SOLID);
  Paint_DrawString_EN(3, 5, "CURRENT_MENU", &Font20, WHITE, BLACK);
  
  Paint_DrawString_EN(10, 40,  "* MENU_OPTION_1", &Font16, WHITE, BLACK);
  Paint_DrawString_EN(10, 60,  "* MENU_OPTION_2", &Font16, BLACK, WHITE);
  Paint_DrawString_EN(10, 80,  "* MENU_OPTION_3", &Font16, WHITE, BLACK);
  Paint_DrawString_EN(10, 100, "* MENU_OPTION_4", &Font16, WHITE, BLACK);
  Paint_DrawString_EN(10, 120, "* MENU_OPTION_5", &Font16, WHITE, BLACK);
  
  Paint_DrawLine(2, 152, 263, 152, BLACK, DOT_PIXEL_2X2, LINE_STYLE_SOLID);
  Paint_DrawString_EN(27, 160, buff, &Font16, WHITE, BLACK);
  
  EPD_2IN7_Display(BlackImage);
  DEV_Delay_ms(2000);

  return;
}

int EPD_2in7_draw_currMenu(){

  if(EPD_2in7_draw_menu(curentMenu, selected)) return 1;
  else return 0;

}

int EPD_2in7_draw_menu(Menu * menu, int state){

  Menu * toDraw = menu;

  char timeBuff[50];
  time_t t = time(NULL);
  struct tm tm = *localtime(&t);
  sprintf(timeBuff, "%d-%02d-%02d %02d:%02d:%02d", tm.tm_year + 1900, tm.tm_mon + 1, tm.tm_mday, tm.tm_hour, tm.tm_min, tm.tm_sec);

  printf("Painting %s\n", toDraw->name);
  Paint_NewImage(BlackImage, EPD_2IN7_WIDTH, EPD_2IN7_HEIGHT, 270, WHITE);
  Paint_SelectImage(BlackImage);
  Paint_Clear(WHITE);

  Paint_DrawLine(1, 30, 264, 30, BLACK, DOT_PIXEL_1X1, LINE_STYLE_SOLID);
  Paint_DrawString_EN(3, 5, toDraw->name, &Font20, WHITE, BLACK);

  for(int i = 0; toDraw->children[i] != NULL; i++){
  
    if(i != state) Paint_DrawString_EN(10, 40 + i * 20, ((Menu *)toDraw->children[i])->name , &Font16, WHITE, BLACK);
    else Paint_DrawString_EN(10, 40 + i * 20, ((Menu *)toDraw->children[i])->name , &Font16, BLACK, WHITE);
    
  }

  Paint_DrawLine(2, 152, 263, 152, BLACK, DOT_PIXEL_2X2, LINE_STYLE_SOLID);
  Paint_DrawString_EN(27, 160, timeBuff, &Font16, WHITE, BLACK);

  EPD_2IN7_Display(BlackImage);
  DEV_Delay_ms(2000);

  return 0;
}

char * EPD_2IN7_curentMenuName(){
  
  return curentMenu->name;
}

int EPD_2in7_free(){

  menu_free(mainMenu);
  EPD_2IN7_Sleep();
  free(BlackImage);
  BlackImage = NULL;
  DEV_Delay_ms(2000);//important, at least 2s
  printf("close 5V, Module enters 0 power consumption ...\n");
  DEV_Module_Exit();

  return 0;
}

Menu * menu_init(Menu * parent, int * menuSize, char ** menuName, int inIndex){

  int index = inIndex;
  Menu * x = (Menu *) malloc(sizeof(Menu));
  x->name = menuName[0];
  x->parent = parent;
  
  x->children = (void **) malloc(sizeof(Menu*) * (menuSize[0] + 1)); 
  x->nChildren = menuSize[0];
  for(int i = 0; i < menuSize[0]; i++) {
    printf("Creating Menu %s\n", menuName[i + inIndex]);
    x->children[i] = menu_init(x, menuSize + i + inIndex, menuName + i + inIndex, index + menuSize[i] - 1);
    index = index + menuSize[i] - 1;
  }
  x->children[menuSize[0]] = NULL;
  
  printf("Menu %s created\n", x->name);
  return x;
}

void menu_free(Menu * menu){

  for(int i = 0; menu->children[i] != NULL; i++){
    printf("Freeing %s\n", ((Menu *)menu->children[i])->name);
    menu_free(menu->children[i]);
  
  }
  printf("Freed %s\n", menu->name);
  free(menu);

  return;
}

Menu * get_subMenu(int * menuNum, Menu * menu){

  if(menuNum[0] == -1) return menu;
  else return get_subMenu(menuNum + 1, menu->children[menuNum[0]]);

}

/*input   0->select
          1->back
          2->up
          3->down
  output  0->OK
          1->out of bounds
          2->no parent/children
*/
int EPD_2IN7_input(int input){

  switch (input)
  {
  case 0:   
    if(curentMenu->children[selected] != NULL){
      curentMenu = curentMenu->children[selected];
      selected = 0;
      EPD_2in7_draw_currMenu();
    }
    break;

  case 1:
    if(curentMenu->parent != NULL){
        curentMenu = curentMenu->parent;
        selected = 0;
        EPD_2in7_draw_currMenu();
    }
    break;

  case 2:
    if(selected > 0){
      selected--;
      EPD_2in7_draw_currMenu();
    }
    break;

  case 3:
    if(selected < curentMenu->nChildren - 1){
      selected++;
      EPD_2in7_draw_currMenu();
    }
    break;

  default:
    break;
  }

  return 0;
}


/*
    Paint_DrawPoint(10, 80, BLACK, DOT_PIXEL_1X1, DOT_STYLE_DFT);
    Paint_DrawPoint(10, 90, BLACK, DOT_PIXEL_2X2, DOT_STYLE_DFT);
    Paint_DrawPoint(10, 100, BLACK, DOT_PIXEL_3X3, DOT_STYLE_DFT);
    Paint_DrawLine(20, 70, 70, 120, BLACK, DOT_PIXEL_1X1, LINE_STYLE_SOLID);
    Paint_DrawLine(70, 70, 20, 120, BLACK, DOT_PIXEL_1X1, LINE_STYLE_SOLID);
    Paint_DrawRectangle(20, 70, 70, 120, BLACK, DOT_PIXEL_1X1, DRAW_FILL_EMPTY);
    Paint_DrawRectangle(80, 70, 130, 120, BLACK, DOT_PIXEL_1X1, DRAW_FILL_FULL);
    Paint_DrawCircle(45, 95, 20, BLACK, DOT_PIXEL_1X1, DRAW_FILL_EMPTY);
    Paint_DrawCircle(105, 95, 20, WHITE, DOT_PIXEL_1X1, DRAW_FILL_FULL);
    Paint_DrawLine(85, 95, 125, 95, BLACK, DOT_PIXEL_1X1, LINE_STYLE_DOTTED);
    Paint_DrawLine(105, 75, 105, 115, BLACK, DOT_PIXEL_1X1, LINE_STYLE_DOTTED);
    Paint_DrawString_EN(10, 0, "waveshare", &Font16, BLACK, WHITE);
    Paint_DrawString_EN(10, 20, "hello world", &Font12, WHITE, BLACK);
    Paint_DrawNum(10, 33, 123456789, &Font12, BLACK, WHITE);
    Paint_DrawNum(10, 50, 987654321, &Font16, WHITE, BLACK);
    Paint_DrawString_CN(130, 0,"���abc", &Font12CN, BLACK, WHITE);
    Paint_DrawString_CN(130, 20, "΢ѩ����", &Font24CN, WHITE, BLACK);
    */