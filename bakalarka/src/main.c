﻿/*Author: Miroslav Krýcha

  This program is writen for Raspberry pi Zero and Zero 2. Its main purpose is to run 
  multiple individualy adresable LED strips. However this porgram does not contain any
  driver functions for LED strips. Those have to be suplied externaly (see folder /led).
  This code manages those programs a resources that they use. It can manualy start them,
  stop or delete them. It can also make them start with interups from GPIO (buttons, 
  digital sensors etc.). 

  This was made for my bachelor work.

  Adding new type of LED strip:
    1. Add new state to void mainAutomaton() function;
    2. Add the new type name to enum LED_strip;
    3. Add new exec string and expand int createProgram(Slot * slot) function;
    4. Add the new type name to printSlot(slots[i]) function;
    5. Add your program to /led folder
  Or You can setup custom type so run you led strip. See mainAutomaton()->custom

  Stuff to be added:
    IR-controls
    Sensor control
    Delete string
    Comments :(
    Revamp E-ink display
    Interupt revamp
    Remove old and useless code
*/

#include <stdlib.h> 
#include <signal.h> 
#include "EPD.h" 
#include <wiringPi.h>
#include <limits.h>

#define EPD_ENABLED TRUE //Enables e-ink display

#define KEY_UP      5 //1. key on epd disp connected to GPIO5
#define KEY_DOWN    6 //2. key on epd disp connected to GPIO6
#define KEY_CONFIRM 13 //3. key on epd disp connected to GPIO13
#define KEY_BACK    19 //4. key on epd disp connected to GPIO19

#define SLOT1_DATA 18
#define SLOT1_CLOCK 23
#define SLOT1_DMA 8
#define SLOT1_RELE_ON 2
#define SLOT1_RELE_VOLTAGE 9

#define SLOT2_DATA 21
#define SLOT2_CLOCK 26
#define SLOT2_DMA 9
#define SLOT2_RELE_ON 3
#define SLOT2_RELE_VOLTAGE 7

#define SLOT3_DATA 14
#define SLOT3_CLOCK 15
#define SLOT3_DMA -1
#define SLOT3_RELE_ON 4
#define SLOT3_RELE_VOLTAGE 12

#define SOUND_GPIO 16 
#define SOUND_OFF 0
#define SOUND_ON 1

#define LIGHT_GPIO 20
#define LIGHT_OFF 0
#define LIGHT_DAY 1
#define LIGHT_NIGHT 2

#define RELE_OFF 1
#define RELE_ON 0

#define SET_5V 1
#define SET_12V 0

#define DISP_REFRESH_LOCK 0
#define DISP_SHOW_LOCK 1

#define FREE_GPIO 13 //Number of Gpio port that are free to be used by LED Strips
#define FREE_DMA 7  //Free Dma chanels to be used by LED strips
#define FREE_PWM 2  //Free pwm gpio

#define DEBUG 1 //1-> debug active 2->debug inactive

typedef enum{ // Types of supported LED strips

  WS281X,
  APA102,
  notUsed

} LED_strip;
#define LED_stripC 3
char * LED_strip_strings[LED_stripC] = {"WS281X", "APA102", "Not used"};
LED_strip LED_strip_types[LED_stripC] = {WS281X, APA102, notUsed};

typedef struct Slot{

  int pid; //program id of detached led program
  char program[256]; //{abspath or relative path to led program} {input variables}
  LED_strip stripT; //type of led strip instaled

  int dma_chanel; //dma chanell used by led strip (-1 if not used)
  int led_count; //number of led on strip
  int led_brigtness; //brightness of led strip in % [0-100] (0->0%, 50->50% etc)
  int led_mode; //selects mode of blinking for leds

  int sound_activated; //If 1 the strip will toggle on sound
  int light_activated; //0-> slot does not respond 1-> on during day 2-> on during night

  int rele_on; //0->OFF; 1->ON
  int rele_voltage; //0->5V; 1->12V

  int rele_on_port;
  int rele_voltage_port;

  int data;  //Data pin
  int clock; //Clock pin (-1 if unused)
} Slot;

int init(); //inicialization function. Must be runed first
void finish(); //last function to be called before exiting

void mainAutomaton(); 

void INT_KEY_UP(); //interupt for e-ink key
void INT_KEY_DOWN(); //interupt for e-ink key
void INT_KEY_CONFIRM(); //interupt for e-ink key
void INT_KEY_BACK(); //interupt for e-ink key
void resolve_key_press(); //obsolete needs to be changed

PI_THREAD (dispTread); //tread function to handle key interupst
void refreshDisp();

void set_GPIO(int gpip, int value);

void EXIT_HANDLER(int signo); //exit handler for CRL + C
int getIntInput(); //gets one int from cmd line and clears input buffer
void clearConsole(); //clears console

int getFreePWM(); //Enables user to select free pwm port thru console
int isPWMfree(int gpio); //Returns TRUE if gpio is unused/free else returns FALSE
void showFreePWM(); //Dumps free PWM ports to console
void getPWM(int gpio); //Marks PWM gpio as used
void freePWM(int gpio); //Marks PWM gpio as free

int getFreeGPIO(); //Enables user to select free GPIO port thru console
int isGPIOfree(int gpio); //Returns TRUE if gpio is unused/free else returns FALSE
void showFreeGPIO(); //Dumps free GPIOs to console
void getGPIO(int gpio); //Marks gpio as used
void freeGPIO(int gpio); //Marks gpio as free

int getFreeDMA(); //Enables user to select free DMA chanel thru console
int isDMAfree(int gpio); //Returns TRUE if DMA chanel is unused/free else returns FALSE
void showFreeDMA(); //Dumps free DMA chanesl to console
void getDMA(int gpio); //Marks DMA chanel as used
void freeDMA(int gpio); //Marks DMA chanel as free

int createProgram(Slot * slot); //Creates Slot->Program from other atributes
int startSlot(Slot * slot); //Starts Slot
int stopSlot(Slot * slot); //Stop Slot
int deleteSlot(Slot * slot); //Deletes Slot
int toggleSlot(int slotNumber); //Toggles slot on/off

int getPath(char * out_strig); //Gets path to existing and readable file
int getArguments(char * out_string); //Gets string of arguments

void printSlot(Slot * slot); //Prints one slot to stdout
void showSlots(); //Prints all slots to stdout
char * charSlot(Slot * slot);

void setVoltage(Slot * slot);

char * menuName[] = {"Main menu", "Toggle light", "Light Modes", "Games", "Glow", "Chase", "Rainbow", "inbetween", "Simon says"}; //Obsolete
int menuSize[] = {3,0,3,2,0,0,0,0,0}; //Obsolete

char * dispName[4] = {"Test"};

int dispSize[] = {3,0,0,0};

unsigned int disp_sel = 0;

int GPIO[FREE_GPIO] = {18, 21, 4, 22, 9, 26, 14, 15, 2, 23, 7, 3, 12}; //GPIO that can be used to run LED strips
int usedGpio[FREE_GPIO] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}; //if usedGpio[0] == 0 -> Gpio[0] is not used; if usedGpio[0] != 0 -> Gpio[0] is used;

int DMA[FREE_DMA] = {8, 9, 10, 11, 12, 13, 14}; //DMA chanels that can be used to run LED strips
int usedDMA[FREE_DMA] = {0, 0, 0, 0, 0, 0, 0}; //same as UsedGpio

int PWM[FREE_PWM] = {18, 21}; //PWM GPIO that can be used to run LED strips
int usedPWM[FREE_PWM] = {0, 0}; //same as UsedGpio

//Strings used to launch LED programs
char * ws281xExecString = "python3.9 ./led/python/WS281X.py %d %d %d %d %d & echo $! > pid"; //LED_COUNT, PWM_PIN, DMA_CHANELL, LED_BRIGTNESS, LED_MODE
char * apa102ExecString = "python3.9 ./led/python/APA102.py %d %d %d %d %d & echo $! > pid"; //LED_COUNT, DATA_PINT, CLOCK_PIN, LED_BRIGTNESS, LED_MODE
char * killString = "sudo kill %d";

int key_pressed; //should be atomic

//Array of slots used to manage led strips
Slot slots[] = {
  {-1, "", notUsed, SLOT1_DMA, 0, 0, 0, SOUND_OFF, LIGHT_OFF, RELE_OFF, SET_5V, SLOT1_RELE_ON, SLOT1_RELE_VOLTAGE, SLOT1_DATA, SLOT1_CLOCK},
  {-1, "", notUsed, SLOT2_DMA, 0, 0, 0, SOUND_OFF, LIGHT_OFF, RELE_OFF, SET_5V, SLOT2_RELE_ON, SLOT2_RELE_VOLTAGE, SLOT2_DATA, SLOT2_CLOCK},
  {-1, "", notUsed, SLOT3_DMA, 0, 0, 0, SOUND_OFF, LIGHT_OFF, RELE_OFF, SET_5V, SLOT3_RELE_ON, SLOT3_RELE_VOLTAGE, SLOT3_DATA, SLOT3_CLOCK}
};
#define slotsC 3 //Number of slots used


//---------------------------------------------------------------------------------------------
int main(void)
{
  if(init() != 0){
    printf("Init failed\n");
    return 1;    
  }

  mainAutomaton();
    
  finish();

  return 0;
}
//---------------------------------------------------------------------------------------------
void INT_KEY_UP(){

  key_pressed = KEY_UP;
  piUnlock (DISP_REFRESH_LOCK);
  return;

}
//---------------------------------------------------------------------------------------------
void INT_KEY_DOWN(){

  key_pressed = KEY_DOWN;
  piUnlock (DISP_REFRESH_LOCK);
  return;

}
//---------------------------------------------------------------------------------------------
void INT_KEY_CONFIRM(){

  key_pressed = KEY_CONFIRM;
  piUnlock (DISP_REFRESH_LOCK);
  return;

}
//---------------------------------------------------------------------------------------------
void INT_KEY_BACK(){

  key_pressed = KEY_BACK;
  piUnlock (DISP_REFRESH_LOCK);
  return;

}
//---------------------------------------------------------------------------------------------
void INT_LIGHT_BOTH(){

  if(DEBUG) printf("void INT_LIGHT_BOTH()\n");
  
  unsigned static int last_light = -100;
  
  if(millis() - last_light < 100) return;
  else last_light = millis();
  
  if(digitalRead(LIGHT_GPIO) == 0){ //FALLING EDGE

    for(int i = 0; i < slotsC; i++){
      if(slots[i].light_activated == LIGHT_NIGHT){
        slots[i].rele_on = 1;
        set_GPIO(slots[i].rele_on_port, slots[i].rele_on);
      }
      
      else if (slots[i].light_activated == LIGHT_DAY){
        slots[i].rele_on = 0;
        set_GPIO(slots[i].rele_on_port, slots[i].rele_on);
      }
    }
  }
  else if(digitalRead(LIGHT_GPIO) == 1){ //RISING EDGE

    for(int i = 0; i < slotsC; i++){
      if(slots[i].light_activated == LIGHT_DAY){
        slots[i].rele_on = 1;
        set_GPIO(slots[i].rele_on_port, slots[i].rele_on);
      }
      
      else if (slots[i].light_activated == LIGHT_NIGHT){
        slots[i].rele_on = 0;
        set_GPIO(slots[i].rele_on_port, slots[i].rele_on);
      }
    }
  }
  return;
}
//---------------------------------------------------------------------------------------------
void INT_SOUND_FALLING(){

  unsigned static int last_sound = -500;
  if(millis() - last_sound < 500) return;
  else last_sound = millis();

  if(DEBUG) printf("void INT_SOUND_FALLING()\n");
  for(int i = 0; i < slotsC; i++){
    if(slots[i].sound_activated == SOUND_ON){
      slots[i].rele_on = !slots[i].rele_on;
      set_GPIO(slots[i].rele_on_port, slots[i].rele_on);
    }
  }
  return;
}
//---------------------------------------------------------------------------------------------
void EXIT_HANDLER(int signo){

    //System Exit
    printf("\r\nHandler:exit\r\n");
    finish();

    stopSlot(&slots[0]);
    stopSlot(&slots[1]);
    stopSlot(&slots[2]);

    exit(0);
}
//---------------------------------------------------------------------------------------------
int init(){

  // Exception handling:ctrl + c
  signal(SIGINT, EXIT_HANDLER);
 
  dispName[3] = charSlot(&slots[0]);
  dispName[2] = charSlot(&slots[1]);
  dispName[1] = charSlot(&slots[2]);

 

  //display init
  if(EPD_ENABLED) EPD_2in7_init(dispSize, dispName);
   
  //key setup
  if(wiringPiSetupGpio() < 0) return 1;
  key_pressed = 0;
  //EPD KEY's settup
  pinMode(KEY_UP, INPUT);
  pinMode(KEY_DOWN, INPUT);
  pinMode(KEY_CONFIRM, INPUT);
  pinMode(KEY_BACK, INPUT);
  //pull up
  pullUpDnControl(KEY_UP, PUD_UP);
  pullUpDnControl(KEY_DOWN, PUD_UP);
  pullUpDnControl(KEY_CONFIRM, PUD_UP);
  pullUpDnControl(KEY_BACK, PUD_UP);
  //key interupts
  if(wiringPiISR(KEY_UP, INT_EDGE_FALLING, &INT_KEY_UP) < 0) return 1;
  if(wiringPiISR(KEY_DOWN, INT_EDGE_FALLING, &INT_KEY_DOWN) < 0) return 1;
  if(wiringPiISR(KEY_CONFIRM, INT_EDGE_FALLING, &INT_KEY_CONFIRM) < 0) return 1;
  if(wiringPiISR(KEY_BACK, INT_EDGE_FALLING, &INT_KEY_BACK) < 0) return 1;
  //relay pins settup
  pinMode(SLOT1_RELE_ON, OUTPUT);
  pinMode(SLOT1_RELE_VOLTAGE, OUTPUT);
  pinMode(SLOT2_RELE_ON, OUTPUT);
  pinMode(SLOT2_RELE_VOLTAGE, OUTPUT);
  pinMode(SLOT3_RELE_ON, OUTPUT);
  pinMode(SLOT3_RELE_VOLTAGE, OUTPUT);
  //Turn off relays
  digitalWrite(SLOT1_RELE_ON, 1);
  digitalWrite(SLOT1_RELE_VOLTAGE, 1);
  digitalWrite(SLOT2_RELE_ON, 1);
  digitalWrite(SLOT2_RELE_VOLTAGE, 1);
  digitalWrite(SLOT3_RELE_ON, 1);
  digitalWrite(SLOT3_RELE_VOLTAGE, 1);
  //Light sensor settup
  pinMode(LIGHT_GPIO, INPUT);
  pullUpDnControl(LIGHT_GPIO, PUD_UP);
  if(wiringPiISR(LIGHT_GPIO, INT_EDGE_BOTH, &INT_LIGHT_BOTH) < 0) return 1;
  //Sound sensor settup
  pinMode(SOUND_GPIO, INPUT);
  pullUpDnControl(SOUND_GPIO, PUD_UP);
  if(wiringPiISR(SOUND_GPIO, INT_EDGE_FALLING, &INT_SOUND_FALLING) < 0) return 1;

  int x = piThreadCreate(dispTread);
  if(x != 0) return 1;

  return 0;
}
//---------------------------------------------------------------------------------------------
void finish(){

  if(EPD_ENABLED) EPD_2in7_free();

  return;
}
//---------------------------------------------------------------------------------------------
void set_GPIO(int gpio, int value){
  printf("Digital write to %d of %d\n", gpio, value);
  digitalWrite(gpio, value);
  return;
}
//---------------------------------------------------------------------------------------------
void setVoltage(Slot * slot){

  if(slot->stripT == APA102) {
    printf("SET VOLTAGE TO 5");
    set_GPIO(slot->rele_voltage_port, SET_5V);
  }
  else if(slot->stripT == WS281X){
    printf("SET VOLTAGE TO 12");
    set_GPIO(slot->rele_voltage_port, SET_12V);
  }
  return;
}
//---------------------------------------------------------------------------------------------
void toggle_light(Slot * slot){

  slot->light_activated++;
  if(slot->light_activated > 2) slot->light_activated = 0;

  return;
}
//---------------------------------------------------------------------------------------------
void toggle_sound(Slot * slot){

  slot->sound_activated++;
  if(slot->sound_activated > 1) slot->sound_activated = 0;

  return;
}
//---------------------------------------------------------------------------------------------
void toggle_mode(Slot * slot){

  slot->led_mode++;
  if(slot->led_mode > 5) slot->led_mode = 0;

  return;
}
//---------------------------------------------------------------------------------------------
void resolve_key_press(){

  if(key_pressed == KEY_UP) EPD_2IN7_input(2);
  else if(key_pressed == KEY_DOWN) EPD_2IN7_input(3); 

  else if(key_pressed == KEY_CONFIRM){
      slots[getSelected()].rele_on = !(slots[getSelected()].rele_on);
      set_GPIO(slots[getSelected()].rele_on_port, slots[getSelected()].rele_on);

  }
  else if(key_pressed == KEY_BACK){
      toggle_mode(&slots[getSelected()]);
      stopSlot(&slots[getSelected()]);
      startSlot(&slots[getSelected()]);
  }

  key_pressed = 0;
  return;
}
//---------------------------------------------------------------------------------------------
int getIntInput(int min, int max ){

  int output;

  while(TRUE){
    if(scanf("%d", &output) != 1){

      printf("Input is not a number\n");
      clearConsole();
      continue;
    }
    if(output > max || output < min){
     
      printf("Input not within bounds [%d-%d]\n", min, max);
      clearConsole();
    }
    else break;
  }
  
  clearConsole();
  return output;
}
//---------------------------------------------------------------------------------------------
int getFreePWM(){

  int out = -1;

  while(TRUE){

    out = getIntInput(0, 40);
    if(isPWMfree(out)) break;
    else printf("PWM used or invalid\n");
  }
  getPWM(out);
  return out;
}
//---------------------------------------------------------------------------------------------
int isPWMfree(int gpio){

  int free = FALSE;

  for(int i = 0; i < FREE_PWM; i++) 
    if(gpio == PWM[i] && !usedPWM[i] && !usedGpio[i]) 
      free = TRUE;

  return free;
}
//---------------------------------------------------------------------------------------------
void showFreePWM(){

  printf("( Free PWM GPIO: ");
  int anyFree = FALSE;

  for(int i = 0; i < FREE_PWM; i++){

    if(!usedPWM[i] && !usedGpio[i]){

      printf("%d ", PWM[i]);
      anyFree = TRUE;
    }
  }
  if(anyFree) printf(")\n");
  else printf("None)\n");
  return;
}
//---------------------------------------------------------------------------------------------
void getPWM(int gpio){

  for(int i = 0; i < FREE_PWM; i++)
    if(PWM[i] == gpio){

      usedGpio[i] = TRUE;
      usedPWM[i] = TRUE;
    }
  return;
}
//---------------------------------------------------------------------------------------------
void freePWM(int gpio){

  for(int i = 0; i < FREE_PWM; i++)
    if(PWM[i] == gpio) {

      usedGpio[1] = FALSE;
      usedPWM[i] = FALSE;
    }
  return;
}
//---------------------------------------------------------------------------------------------
int getFreeDMA(){

  int out = -1;

  while(TRUE){

    out = getIntInput(0, 40);
    if(isDMAfree(out)) break;
    else printf("DMA used or invalid\n");
  }
  getDMA(out);
  return out;
}
//---------------------------------------------------------------------------------------------
int isDMAfree(int dma){

  int free = FALSE;

  for(int i = 0; i < FREE_DMA; i++) 
    if(dma == DMA[i] && !usedDMA[i]) 
      free = TRUE;

  return free;
}
//---------------------------------------------------------------------------------------------
void showFreeDMA(){

  printf("( Free DMA channels: ");
  int anyFree = FALSE;

  for(int i = 0; i < FREE_DMA; i++){

    if(!usedDMA[i]){

      printf("%d ", DMA[i]);
      anyFree = TRUE;
    }
  }
  if(anyFree) printf(")\n");
  else printf("None )\n");
  return;
}
//---------------------------------------------------------------------------------------------
void getDMA(int dma){

  for(int i = 0; i < FREE_DMA; i++)
    if(DMA[i] == dma) usedDMA[i] = TRUE;

  return;
}
//---------------------------------------------------------------------------------------------
void freeDMA(int dma){

  for(int i = 0; i < FREE_DMA; i++)
    if(DMA[i] == dma) usedDMA[i] = FALSE;

  return;
}
//---------------------------------------------------------------------------------------------
int getFreeGPIO(){

  int out = -1;

  while(TRUE){

    out = getIntInput(0, 40);
    if(isGPIOfree(out)) break;
    else printf("GPIO pin used or invalid\n");
  }
  getGPIO(out);
  return out;
}
//---------------------------------------------------------------------------------------------
int isGPIOfree(int gpio){

  int free = FALSE;

  for(int i = 0; i < FREE_GPIO; i++) 
    if(gpio == GPIO[i] && !usedGpio[i]) 
      free = TRUE;

  return free;
}
//---------------------------------------------------------------------------------------------
void showFreeGPIO(){

  printf("( Free GPIO pins: ");
  int anyFree = FALSE;

  for(int i = 0; i < FREE_GPIO; i++){

    if(!usedGpio[i]){

      printf("%d ", GPIO[i]);
      anyFree = TRUE;
    }
  }
  if(anyFree) printf(")\n");
  else printf("None )\n");
  return;
}
//---------------------------------------------------------------------------------------------
void getGPIO(int gpio){

  for(int i = 0; i < FREE_GPIO; i++)
    if(GPIO[i] == gpio){

      usedGpio[i] = TRUE;
    }
  return;
}
//---------------------------------------------------------------------------------------------
void freeGPIO(int gpio){

  for(int i = 0; i < FREE_GPIO; i++)
    if(GPIO[i] == gpio) {

      usedGpio[i] = FALSE;
    }
  return;
}
//---------------------------------------------------------------------------------------------
void clearConsole(){
  int c;
  while ((c = getchar()) != '\n' && c != EOF) { }
  return;
}
//---------------------------------------------------------------------------------------------
void refreshDisp(){

  if(dispName[1] == NULL || dispName[2] == NULL || dispName[3] == NULL) return;

  free(dispName[1]);
  free(dispName[2]);
  free(dispName[3]);

  dispName[1] = charSlot(&slots[0]);
  dispName[2] = charSlot(&slots[1]);
  dispName[3] = charSlot(&slots[2]);

  piLock(DISP_SHOW_LOCK);
  EPD_2in7_draw_currMenu();
  piUnlock(DISP_SHOW_LOCK);
}
//
int createProgram(Slot * slot){

  switch (slot->stripT){

    case WS281X:
      snprintf(slot->program, 256, ws281xExecString, slot->led_count, slot->data, 
               slot->dma_chanel, slot->led_brigtness, slot->led_mode);
      break;

    case APA102:
      snprintf(slot->program, 256, apa102ExecString, slot->led_count, slot->data, 
               slot->clock, slot->led_brigtness, slot->led_mode);
      break;

    default:
      break;
  }
  return 0;
}
//---------------------------------------------------------------------------------------------
int startSlot(Slot * slot){

  if(slot->stripT == notUsed) return 0;

  createProgram(slot);

  if(DEBUG) printf("System: %s\n", slot->program);

  system(slot->program);
  FILE * fp = fopen("./pid", "r");
  fscanf(fp,"%d", &(slot->pid));
  fclose(fp);

  printf("Stated program PID: %d\n", slot->pid);

  return 0;
}
//---------------------------------------------------------------------------------------------
int stopSlot(Slot * slot){

  char killBuff[64];

  if(slot->stripT == notUsed) return 0;

  if(slot->pid != -1){

    snprintf(killBuff, 64, killString, slot->pid);
    if(DEBUG) printf("System: %s\n", killBuff);
    system(killBuff);

    slot->pid = -1;
  }

  return 0;
}
//---------------------------------------------------------------------------------------------
int toggleSlot(int slotNumber){

  if(slotsC == 0) return 1;

  Slot * x = &slots[slotNumber - 1];

  if(x->pid == -1) startSlot(x);
  else stopSlot(x);

  return 0;
}
//---------------------------------------------------------------------------------------------
int getPath(char * out_string){

  return 0;
}
//---------------------------------------------------------------------------------------------
int getArguments(char * out_string){

  return 0;
}
//---------------------------------------------------------------------------------------------
void showSlots(){

  for(int i = 0; i < slotsC; i++){
    
    printf("%d. ", i + 1);
    printSlot(&slots[i]);
    printf("\n");
  }

  return;
}
//---------------------------------------------------------------------------------------------
void printSlot(Slot * slot){

  char * type;

  if(slot == NULL) return;
  else if(slot->stripT == WS281X) type = "WS281X";
  else if(slot->stripT == APA102) type = "APA102";
  else if(slot->stripT == notUsed) type = "NotUsed";
  else type = "????";

  printf("LED strip %s of %d LEDs, in mode %d", type, slot->led_count, slot->led_mode);

  return;
}
//---------------------------------------------------------------------------------------------
char * charSlot(Slot * slot){

  char * retStr = (char *) malloc(sizeof(char) * 128);

  if(slot == NULL){
    free(retStr);
    return NULL;
  }
  else if(slot->stripT == WS281X){
    snprintf(retStr, 128, "LED strip WS281X of %d LEDs, in mode %d", slot->led_count, slot->led_mode);
    return retStr;
  }
  else if(slot->stripT == APA102){
    snprintf(retStr, 128, "LED strip APA102 of %d LEDs, in mode %d", slot->led_count, slot->led_mode);
    return retStr;
  }
  else if(slot->stripT == notUsed){
    snprintf(retStr, 128, "Slot is not used.");
    return retStr;
  }
  else{
    snprintf(retStr, 128, "???????");
    return retStr;
  }

  return NULL;
}
//---------------------------------------------------------------------------------------------
void inline printLightControl(int x, int pos){

  if(x == 0) printf("%d. No Light Control\n", pos);
  else if(x == 1) printf("%d. On during day\n", pos);
  else if(x == 2) printf("%d. On during night\n", pos);

}
//---------------------------------------------------------------------------------------------
void inline printSoundControl(int x, int pos){

  if(x == 0) printf("%d. No Sound Control\n", pos);
  else if(x == 1) printf("%d. Toggle on sound\n", pos);

}
//---------------------------------------------------------------------------------------------
void inline printOnOff(int x, int pos){

  if(x == RELE_OFF) printf("%d. OFF\n", pos);
  else if(x == RELE_ON) printf("%d. ON\n", pos);

}
//---------------------------------------------------------------------------------------------
void inline printMode(int x, int pos){

  printf("%d. Mode: %d\n", pos, x);

}
//---------------------------------------------------------------------------------------------
void inline printLedCount(int x, int pos){

  printf("%d. LED's: %d\n", pos, x);

}
//---------------------------------------------------------------------------------------------
void inline printBrigtness(int x, int pos){

  printf("%d. Brigtness: %d\n", pos, x);

}
//---------------------------------------------------------------------------------------------
void inline printAll_LED_strip(int pos){

  for(int i = 0; i < LED_stripC; i++){

    printf("%d. %s\n", pos + i, LED_strip_strings[i]);

  }

}
//---------------------------------------------------------------------------------------------
void inline printLedType(LED_strip type, int pos){

  for(int i = 0; i < LED_stripC; i++){

    if(type == LED_strip_types[i]) printf("%d. %s\n", pos, LED_strip_strings[i]);

  }

}
//---------------------------------------------------------------------------------------------
PI_THREAD (dispTread){

  EPD_2in7_draw_currMenu();

  for(;;){
    piLock(DISP_REFRESH_LOCK);
    piLock(DISP_SHOW_LOCK);

    resolve_key_press();
    piUnlock(DISP_SHOW_LOCK);

  }

  return 0;
}
//---------------------------------------------------------------------------------------------
void mainAutomaton(){

  typedef enum{ //posible states of automaton

    start,
    add_strip,
    toggle_strip,
    remove_strip,
    automation,
    add_ws281x,
    add_apa102,
    add_custom,
    slot_control,
    set_strip,
    set_led_count,
    set_brigtness,
    exit

  } state;

  state current_state = start;
  state next_state = start;

  int input; //variable used for int inputs
  int selectedSlot = -1;

  while(TRUE){

    switch(current_state){

      case start:

        if(!DEBUG) system("clear");
        printf("Select one of the folowing:\n");
        for(int i = 0; i < slotsC; i++){
          printf("%d. ", i + 1);
          printSlot(&slots[i]);
          printf("\n");
        }

        selectedSlot = getIntInput(1, slotsC) - 1;

        next_state = slot_control;

        break;

      case slot_control:

        if(!DEBUG) system("clear");
        printf("Select one of the folowing:\n");
        printf("--------Control--------\n");
        printOnOff(slots[selectedSlot].rele_on, 1);
        printMode(slots[selectedSlot].led_mode, 2);
        printLightControl(slots[selectedSlot].light_activated, 3);
        printSoundControl(slots[selectedSlot].sound_activated, 4);
        printf("--------Settings-------\n");
        printLedType(slots[selectedSlot].stripT, 5);
        printLedCount(slots[selectedSlot].led_count, 6);
        printBrigtness(slots[selectedSlot].led_brigtness, 7);
        
        input = getIntInput(0, 7);

        if(input == 0) next_state = start;
        else if(input == 1){
          slots[selectedSlot].rele_on = !(slots[selectedSlot].rele_on);
          set_GPIO(slots[selectedSlot].rele_on_port, slots[selectedSlot].rele_on);
          next_state = slot_control;
        }
        else if(input == 2){
          toggle_mode(&slots[selectedSlot]);
          stopSlot(&slots[selectedSlot]);
          startSlot(&slots[selectedSlot]);
          next_state = slot_control;
        }
        else if(input == 3){
          toggle_light(&slots[selectedSlot]);
          next_state = slot_control;
        }
        else if(input == 4){
          toggle_sound(&slots[selectedSlot]);
          next_state = slot_control;
        }
        else if(input == 5) next_state = set_strip;
        else if(input == 6) next_state = set_led_count;
        else if(input == 7) next_state = set_brigtness;

        break;
      case set_strip:

        printf("Select one of the folowing:\n");
        printAll_LED_strip(1);

        input = getIntInput(0, LED_stripC);

        if(input == 0) next_state = slot_control;
        else{
          //Turn of power
          slots[selectedSlot].rele_on = RELE_OFF;
          set_GPIO(slots[selectedSlot].rele_on_port, slots[selectedSlot].rele_on);

          slots[selectedSlot].stripT = LED_strip_types[input] - 1;

          setVoltage(&slots[selectedSlot]);

          stopSlot(&slots[selectedSlot]);
          startSlot(&slots[selectedSlot]);

          next_state = slot_control;
        }
        
        break;

      case set_led_count:

        printf("Imput number of LED's on strip[1 - %d:\n", INT_MAX); //Naivní maximum :D

        input = getIntInput(1, INT_MAX);

        slots[selectedSlot].led_count = input;

        stopSlot(&slots[selectedSlot]);
        startSlot(&slots[selectedSlot]);

        next_state = slot_control;

        break;

      case set_brigtness:

        printf("Imput number brightness in %% [0 - 100]:"); //Naivní maximum :D

        input = getIntInput(0, 100);

        slots[selectedSlot].led_brigtness = input;

        stopSlot(&slots[selectedSlot]);
        startSlot(&slots[selectedSlot]);

        next_state = slot_control;

        break;
      
      case add_strip:

        if(!DEBUG) system("clear");
        printf("Select one of the folowing:\n");
        printf("1. WS281x led strip\n");
        printf("2. APA102 led strip\n");
        printf("3. Custom led strip\n");

        input = getIntInput(1,3);

        if (input == 0) next_state = start;
        else if(input == 1) next_state = add_ws281x;
        else if(input == 2) next_state = add_apa102;
        else if(input == 3) next_state = add_custom;

        break;

      /*case add_ws281x:

        slots[slotsC] = (Slot *) malloc(sizeof(Slot));

        slots[slotsC].stripT = WS281X;
        slots[slotsC].pid = -1;

        if(!DEBUG) system("clear");
        printf("Enter number of LED on strip:\n");
        slots[slotsC]->led_count = getIntInput(0, INT_MAX);

        printf("Enter PWM GPIO number used for data line:\n");
        showFreePWM();
        slots[slotsC]->data = getFreePWM();

        printf("Enter DMA channel number used for data line:\n");
        showFreeDMA();
        slots[slotsC]->dma_chanel = getFreeDMA();

        printf("Enter desired brightness in %%: \n");
        slots[slotsC]->led_brigtness = getIntInput(0, 100);

        printf("Select Mode\n");
        slots[slotsC]->led_mode = getIntInput(0, 100);

        createProgram(slots[slotsC]);
        startSlot(slots[slotsC]);

        slotsC ++;
        next_state = start;
        break;

      case add_apa102:

        slots[slotsC] = (Slot *) malloc(sizeof(Slot));

        char pathBuff[192];
        char argBuff[64];

        slots[slotsC]->stripT = APA102;
        slots[slotsC]->pid = -1;

        if(!DEBUG) system("clear");
        printf("Enter number of LED on strip:\n");
        slots[slotsC]->led_count = getIntInput(0, INT_MAX);

        printf("Enter GPIO pin number used for data line:\n");
        showFreeGPIO();
        slots[slotsC]->data = getFreeGPIO();

        printf("Enter GPIO pin number used for clock line:\n");
        showFreeGPIO();
        slots[slotsC]->clock = getFreeGPIO();

        printf("Enter path to file to be executed:\n");
        getPath(pathBuff);

        printf("Enter arguments:\n");
        getArguments(argBuff);

        startSlot(slots[slotsC]);

        slotsC ++;
        next_state = start;
        break;

      break;

      case add_custom:

        slots[slotsC] = (Slot *) malloc(sizeof(Slot));

        slots[slotsC]->stripT = APA102;
        slots[slotsC]->pid = -1;

        printf("Enter GPIO pin number used for your custom strip:\n");
        showFreeGPIO();
        slots[slotsC]->data = getFreeGPIO();

        int i = 1;
        while(1){

          printf("Enter aditional GPIO pin number used for your custom strip:\n");
          printf("0. none\n");
          showFreeGPIO();
          slots[slotsC]->data = getFreeGPIO();
          i++;
          if(i >= 8) break;
        }

        printf("Enter DMA channel number used for data line:\n");
        showFreeDMA();
        slots[slotsC]->dma_chanel = getFreeDMA();

        printf("Enter desired brightness in %%: \n");
        slots[slotsC]->led_brigtness = getIntInput(0, 100);

        printf("Select Mode\n");
        slots[slotsC]->led_mode = getIntInput(0, 100);

        createProgram(slots[slotsC]);
        startSlot(slots[slotsC]);

        slotsC ++;
        next_state = start;

      break;

      case toggle_strip:

        if(!DEBUG) system("clear");
        printf("Select strip to toggle ON/OFF:\n");
        showSlots();

        input = getIntInput(0, slotsC);

        if(input == 0){

          next_state = start;
          break;
        }

        toggleSlot(input);
        next_state = toggle_strip;

        break;*/

      default:

        printf("Menu does not exist/is invalid. Fix your shit dammint!!\n");
        return;

        break;
    }

    input = -1;
    current_state = next_state;
    if(next_state == start) refreshDisp();
    if(current_state == exit) break;
  }

  return;
}