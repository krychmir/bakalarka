#!/usr/bin/env python3
"""Ultra simple sample on how to use the library"""
from apa102_pi.driver import apa102
from apa102_pi.colorschemes import colorschemes
import time
import sys

if __name__ == '__main__':

    if len(sys.argv) != 6:
        print("Inputs should be: LED_COUNT DATA_PIN CLOCK_PIN LED_BRIGHTNESS LED_MODE")
        exit()

    LED_COUNT = int(sys.argv[1])        # Number of LED pixels.
    DATA_PIN = int(sys.argv[2])         # GPIO pin connected to the DI pin
    CLOCK_PIN = int(sys.argv[3])        # GPIO pin connected to the CI pin
    LED_BRIGHTNESS = int(sys.argv[4])   # Set to 0 for darkest and 100 for brightest
    LED_MODE = int(sys.argv[5])         # LED mode to be displayed


    try:

        while True:

            if LED_MODE == 0: # All white

                strip = apa102.APA102(num_led=LED_COUNT, bus_method='bitbang', mosi=DATA_PIN, sclk=CLOCK_PIN, global_brightness=31)
                for x in range(60):
                    strip.set_pixel_rgb(x, 0xFFFFFF, bright_percent=LED_BRIGHTNESS)  
                strip.show()

            if LED_MODE == 1: # All red

                strip = apa102.APA102(num_led=LED_COUNT, bus_method='bitbang', mosi=DATA_PIN, sclk=CLOCK_PIN, global_brightness=31)
                for x in range(60):
                    strip.set_pixel_rgb(x, 0xFF0000, bright_percent=LED_BRIGHTNESS)  
                strip.show()

            if LED_MODE == 2: # All green

                strip = apa102.APA102(num_led=LED_COUNT, bus_method='bitbang', mosi=DATA_PIN, sclk=CLOCK_PIN, global_brightness=31)
                for x in range(60):
                    strip.set_pixel_rgb(x, 0x00FF00, bright_percent=LED_BRIGHTNESS)  
                strip.show()

            if LED_MODE == 2: # All blue

                strip = apa102.APA102(num_led=LED_COUNT, bus_method='bitbang', mosi=DATA_PIN, sclk=CLOCK_PIN, global_brightness=31)
                for x in range(60):
                    strip.set_pixel_rgb(x, 0x0000FF, bright_percent=LED_BRIGHTNESS)  
                strip.show()

            elif LED_MODE == 4:
                my_cycle = colorschemes.StrandTest(num_led=LED_COUNT, pause_value=0, num_steps_per_cycle=LED_COUNT, num_cycles=-1, order='rgb', global_brightness=31, bus_method='bitbang', mosi=DATA_PIN, sclk=CLOCK_PIN)
                my_cycle.start()

    except KeyboardInterrupt:
        strip = apa102.APA102(num_led=LED_COUNT, bus_method='bitbang', mosi=DATA_PIN, sclk=CLOCK_PIN, global_brightness=31)
        strip.clear_strip()
        strip.cleanup()



    # Initialize the library and the strip. This defaults to SPI bus 0, order 'rgb' and a very low brightness
    #strip = apa102.APA102(num_led=60, bus_method='bitbang', mosi=14, sclk=15)

    # Turn off all pixels (sometimes a few light up when the strip gets power)
    #strip.clear_strip()
    #my_cycle = colorschemes.StrandTest(num_led=60, pause_value=0, num_steps_per_cycle=60, num_cycles=30, order='rgb', global_brightness=10, bus_method='bitbang', mosi=14, sclk=15)
    #my_cycle.start()
    #while True:

        
        #my_cycle.start()
        
        # Prepare a few individual pixels
        #strip.set_pixel_rgb(12, 0xFF0000)  # Red
        #strip.set_pixel_rgb(24, 0xFFFFFF)  # White
        #strip.set_pixel_rgb(40, 0x00FF00)  # Green

        # Copy the buffer to the Strip (i.e. show the prepared pixels)
        #strip.show()
        #time.sleep(0.1)

    # Clear the strip and shut down
    # strip.clear_strip()
    # strip.cleanup()

